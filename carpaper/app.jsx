import React from 'react'
import {render} from 'react-dom'
import Shell from './client/components/shell.jsx'
import {Router,Route,IndexRoute,browserHistory} from 'react-router'
import Home from './client/components/Home/home.jsx'
import Confirm from './client/components/Confirm/Confirm.jsx'
import Cart from './client/components/Cart/Cart.jsx'
import Checkout from './client/components/Checkout/Checkout.jsx'
import Receipt from './client/components/Receipt/Receipt.jsx'
import Request from './client/components/Request/Request.jsx'
import store,{history} from './store'
import {Provider} from 'react-redux'


const router= (
		<Provider store={store}>
			<Router history={history}>
				<Route path="/" component={Shell}>
					<IndexRoute component={Home}/>
					<Route path="/home" component={Home}/>
					<Route path="/cart" component={Cart}/>
					<Route path="/request" component={Request}/>
					<Route path="/details" component={Checkout}/>
					<Route path="/confirm/:tId" component={Confirm}/>
					<Route path="/receipt/:tId" component={Receipt}/>
					<Route/>
				</Route>
			</Router>
		</Provider>
	);

render(router, document.getElementById('root')); 