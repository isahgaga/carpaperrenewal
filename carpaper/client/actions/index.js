import * as Types from '../constants/ActionTypes'


export const addPapers= papers=>({
	type:Types.ADD_PAPERS,
	papers
})

export const addToCart = paperId => ({
  type: Types.ADD_TO_CART,
  paperId
})

export const removeFromCart = id => ({
  type: Types.REMOVE_FROM_CART,
  id
})

export const setCartInitialState= initialState=>({type:Types.SETCARTINITIALSTATE,
initialState
})