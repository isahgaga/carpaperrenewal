import React from 'react'
import AboutStyle from './About.css'


export default class About extends React.Component{

	render(){
		return (
			<div className="container section-padding" id="about">
    <div className="section">
		  <div className="row">
		    <div className="col-md-5 sm-about">
		      
		      <h4>About Us</h4>
		      <p className="light">At OgaPaper.com, we aim to provide you, and all our clients with a stress and hassle free request and delivery service. Our belief is that we can create an efficient system, in collaboration with all our partners, which allows you to get all your car papers- be it your vehicle license or your drivers license in a seamless manner, while costing little on your pocket.
		      
		        </p>
		    </div>
		     <div className="col-md-6 col-md-offset-1">
		         <div className="video-container">
		        	<iframe width="853" height="480" src="//www.youtube.com/embed/Q8TXgCzxEnw?rel=0" frameBorder="0" allowFullScreen></iframe>
		     	</div>
		    </div>
		   
		  </div>

	</div>
  </div>
			);
	}
}