import React from 'react'
import BenefitCss from './Benefit.css'
import {getBenefits} from '../../services'

export default class Benefit extends React.Component{

  constructor(props){
      super(props);
      this.getBens=this.getBens.bind(this);
    this.state={
      benefits:[]
    };
  }
 
  getBens(){
     return   this.state.benefits.map((benefit,index)=>{
      const TYPEOFBENEFIT=benefit.title ==='Fast'?'flash_on':benefit.title ==='Convenient'?'tag_faces':'money_off';
        return (
          <div className="col-md-4" key={index}>
          <div className="icon-block">
            <h2 className="center teal-text lighten-1-text"><i className="material-icons medium">{TYPEOFBENEFIT}</i></h2>
            <h5 className="center">{benefit.title}</h5>

            <p className="light center">{benefit.benefit}</p>
          </div>
        </div>
          )

          })
  }

	render(){
		return (
			
    <div className="section  benefits section-padding" id="benefits">
    	<div className="container ">
      <div className="row">
      <h4 className="section-h">Why Trust Us?</h4>
        {this.getBens()}
      </div>

    </div>
    

    
  </div>
			);
	}

  componentDidMount(){
    getBenefits().then(data=>{
        this.setState({benefits:data.data.reverse()})
    }).catch(e=>console.log(e));
  }
}