import React from 'react';
import CartStyle  from './Cart.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {removeFromCart} from '../../actions';
import {Link} from 'react-router';


class Cart extends React.Component{
	constructor(props){
      super(props);
      this.getNumberOfCartItems=this.getNumberOfCartItems.bind(this);
      this.getTotal=this.getTotal.bind(this);
      this.getCartItems=this.getCartItems.bind(this);
      this.deleteCartItem=this.deleteCartItem.bind(this);

  }
  getNumberOfCartItems(){
   return Object.values(this.props.cart.quantityById).reduce((total, id) =>
      total + id,
      0
    );
  }
  getTotal(){
  	const {cart}= this.props;
  	const {byId}=this.props.papers;
  let result=  cart.addedIds.reduce((total, id) =>
      total + byId[id].price * cart.quantityById[id],
      0
    )
    .toLocaleString('us',{style:'currency',currency:'NGN'});
    return result.substr(3);
  }
  getCartItems(){
  	const {cart}= this.props;
  	const {byId}=this.props.papers;
  	return	cart.addedIds.map(id=>({...byId[id],price:byId[id].price.toLocaleString('us',{style:'currency',currency:'NGN'}).substr(3),quantity:cart.quantityById[id]}))
  	.map(cartItem=>
  				<li className="collection-item cart-list-item" key={cartItem.id}>
			        <div>
	        			<h5 className="">{cartItem.title}</h5>
	        			<span className="teal-text">&#8358;{cartItem.price}</span> <span className="cart-qty">Quantity: {cartItem.quantity}</span>
	        		</div>
		        	<span className="delete-btn" onClick={this.deleteCartItem.bind(null,cartItem.id)}><i className="material-icons">close</i></span>
		        </li>
  		)
  	}
  	deleteCartItem(id,e){
  			e.preventDefault();
  			console.log(id);
  			this.props.removeFromCart(id);
  			
  	}

	render(){
		return ( 
			<main className="cart-section">
			<section  >
<div className="container ">
					<div className="row no-margin-bottom">
						<div className="col s12 m6 offset-m3" >
							<div className="card white cart-card">
									<ul className="collection with-header">
									        <li className="collection-header "> 
									        	<span className="cart-total teal-text">
									        		<i className="material-icons cart-icon">shopping_cart</i>
          											<span className="new badge white-text" data-badge-caption="">{this.getNumberOfCartItems()}</span>
          										</span>
									        	<span className="cart-total ">Total: <span className="teal-text">&#8358;{this.getTotal()}</span></span>
									        </li>
									        { this.getNumberOfCartItems() < 1 && 

									        	<li className="collection-item cart-list-item">
											        <div>
									        			<span className="empty-cart-msg">No item in cart</span>
									        		</div>
										        </li>
									        }
									       {this.getCartItems()}
							        </ul>
							        		{
							        			this.getNumberOfCartItems() < 1 ? 
							        	<div className="card-action">
							              <Link to="/" className="btn-large waves-effect btn-flat waves-light blue lighten-1">Return home</Link>
							            </div>
							            :
							            <div className="card-action">
							              <Link to="/details" className="btn-large waves-effect btn-flat waves-light teal lighten-1">Checkout</Link>
							            </div>
							        		}
							            
							            
							          </div>
							
						</div>
						</div>
						</div>
				
			</section>
			</main>
			);
	}

	
}

function mapStateToProps(state){
	return {
		papers:state.Papers,
		cart:state.Cart 
	}
}
	
function mapDispatchToProps(dispatch){
	return bindActionCreators({removeFromCart},dispatch);
}

const ShellComponent = connect(mapStateToProps,mapDispatchToProps)(Cart);
export default ShellComponent;