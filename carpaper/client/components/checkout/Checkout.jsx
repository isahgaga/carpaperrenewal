import React,{Component} from 'react'
import CheckoutStyle from './Checkout.css'
//import $ from 'jquery'
import CheckoutFormFunc from '../checkoutForms/FormOne.jsx'
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {getDeliveryPrices} from '../../services';
import localForage from 'localForage';

var deliveryPrices;
var CheckoutForm;
localForage.getItem('details').then(data=>{CheckoutForm=CheckoutFormFunc(data);});



 class Checkout extends Component {
 	constructor(props){
      super(props);
       
  }

	componentDidMount(){
		
		getDeliveryPrices().then(data=>deliveryPrices=data.data);

			setTimeout(function(){
				try{
					$('.tooltipped').tooltip({delay: 50,html:true,position:'left',tooltip:`<div>
      	standard delivery charge for 1 paper=${deliveryPrices.standard.amount}<br>
      	standard delivery charge for 2 papers=${deliveryPrices.standard.amount + 500}<br>
      	standard delivery charge for 3 papers and more=${deliveryPrices.standard.amount + 1000}<br>
      	express delivery(Same day)=standard delivery + 1000
      	</div>`});
				}
      
     catch(e){
		}

    },3000)

		
	
}



	render(){
				return (
			<main className="checkout-section">
			
				<section>
					<div className="container ">
						<div className="row no-margin-bottom">
							<div className="col s12 m6 offset-m3" >
							<div className="card checkout-card">
					            <div className="card-content">
					              <span className="card-title details-card-title"><i className="material-icons">directions_car</i>Vehicle details</span>
					             
					             <CheckoutForm cart={this.props.cart} router={this.props.router}/>
					            </div>
					            
				        	</div>
				        	</div>
						</div>
					</div>
					
				</section>
			</main>			

			);
	}
}

function mapStateToProps(state){
	return {
		cart:state.Cart 
	}
}
	

const ShellComponent = connect(mapStateToProps)(Checkout);
export default ShellComponent;