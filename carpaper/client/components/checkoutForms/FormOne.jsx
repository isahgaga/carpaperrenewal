
import React from 'react'
import { Field, reduxForm ,SubmissionError} from 'redux-form'
import {Link} from 'react-router';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import './FormOne.css'
import {makeBooking} from '../../services';
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import localForage from 'localForage';



const validate = values => {
  const errors = {}
  if (!values.name) {
    errors.name = 'Required'
  } 
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if(!values.owner_name){
  	errors.owner_name='Required'
  }
  if(!values.plate_number){
  	errors.plate_number='Required'
  }
  if(!values.plate_number){
  	errors.plate_number='Required'
  }
  if(!values.vehicle_make){
  	errors.vehicle_make='Required'
  }
  
  if(!values.expiry_date){
    errors.expiry_date='Required'
  }
  
  if(!values.phone){
  	errors.phone='Required'
  }
  if(!values.delivery_address){
  	errors.delivery_address='Required'
  }
  if(!values.delivery_type){
  	errors.delivery_type='Required'
  }
  return errors
}

const submit =function(...others) {
  const [values,dispatch]=others;
  const data={papers:this.props.cart.quantityById,details:values};
  const errors = {};
  localForage.setItem('details',values);
  dispatch(showLoading());
return  makeBooking(data).then(data=>{
   dispatch(hideLoading());
  
  this.props.router.push(`/confirm/${data.data}`);
}).catch(e=>{
     const errorRes=e.response.data;
     
     if (errorRes ==='Invalid coupon' || errorRes==='expired coupon' || errorRes==='coupon limit reached') {
      dispatch(hideLoading());
        throw new SubmissionError({coupon:errorRes});
     }

     if(errorRes.chasis_number){
       errors.chasis_number=errorRes.chasis_number[0];
     }

     if (errorRes.plate_number) {
      errors.plate_number=errorRes.plate_number[0];
    
  } 

  if (errorRes.engine_number) {
    errors.engine_number=errorRes.engine_number[0];
  } 

   if (errorRes.vehicle_make) {
    errors.vehicle_make=errorRes.vehicle_make[0];
  } 

   if (errorRes.owner_name) {
    errors.owner_name=errorRes.owner_name[0];
  }

   if (errorRes.expiry_date) {
    errors.expiry_date=errorRes.expiry_date[0];
  }  
     if (errorRes.name) {
     errors.name=errorRes.name[0];
  } 

   if (errorRes.phone) {
      errors.phone=errorRes.phone[0];  }

   if (errorRes.email) {
    errors.email=errorRes.email[0];
  }  
   if (errorRes.delivery_address) {
    errors.delivery_address=errorRes.delivery_address[0];
  } 
   if (errorRes.delivery_type) {
    errors.delivery_type=errorRes.delivery_type[0];
  }
  dispatch(hideLoading());
  throw new SubmissionError(errors);

  });
  	 
}

const renderField = ({ input, label, type, meta: { touched, error, warning } }) =>{ 

    let hasError=touched && error?'has-error':'';
    let classes= label ==='Coupon(optional)' || label ==='Engine Number(optional)'|| label ==='Chasis Number(optional)'?'input-field coupon-wrapper':'input-field';
 return (
  
  <div className={classes}>
		<input {...input}  type={type} id={input.name} className={hasError}/>
		<label htmlFor={input.name} className={input.value===''?'':'active'}>{label}</label>
		{touched && (error && <span className="error-msg">{error}</span>)}
	</div>
) }


const renderSelect=({ input, label,className, meta: { touched, error, warning } })=>{
    let hasError=touched && error?'has-error2':'';
 return (
<div className={hasError}>
	  			<label htmlFor={input.name}><i className="material-icons delivery-info tooltipped">info</i> {label}</label>
	  			<select {...input} id={input.name} className={className}>
	  				<option value="" disabled selected>Choose a delivery type</option>
			      	<option value="0">Standard(3 days)</option>
			      	<option value="1">Express(24 hours)</option>
	  			</select>
	  			{touched && (error && <span className="error-msg">{error}</span>)}
	  			 	
	  		</div>	    
	)}
const renderDatePicker=({ input, label, type, readonly,selected, meta: { touched, error, warning } })=>{
   let hasError=touched && error?'has-error':'';
   
   delete input.value;
  return (
				<div className="input-field">
					<label htmlFor={input.name} className="active">{label}</label>
          <DatePicker {...input} type={type} id={input.name} className={hasError} dateFormat="MM/DD/YYYY" readOnly={readonly} withPortal={true} selected={selected}/>
					{touched && (error && <span className="error-msg">{error}</span>)}
				</div>
	)
}
class SyncValidationForm extends React.Component{

  constructor(props){
      super(props);
      this.handleDateChange=this.handleDateChange.bind(this);
      this.state={
                    startDate: moment(),
                  };
  }

  handleDateChange(e,n){
     
      this.setState({
      startDate:n
    });
  }
  
  handleDeliveryChange(e,n){
      /*this.setState({
      deliveryPrice:this.deliveryPrices[n.toLower]
    });*/
  }

  render(){
    const { handleSubmit, pristine, reset, submitting ,invalid,dirty,dispatch} = this.props

  return (

    <form onSubmit={handleSubmit}>
    <Field name="owner_name" type="text" component={renderField} label="Owner's Name"/>
    <Field name="plate_number" type="text" component={renderField} label="Plate Number"/>
    <Field name="vehicle_make" type="text" component={renderField} label="Vehicle Make"/>
    <Field name="engine_number" type="text" component={renderField} label="Engine Number(optional)"/>
    <Field name="chasis_number" type="text" component={renderField} label="Chasis Number(optional)" />
    <Field name="expiry_date" type="date" component={renderDatePicker}   label="Expiry Date" readonly={true} 
     onChange={this.handleDateChange}   selected={this.state.startDate}/>
    
    <div className="card-form-padding">
      <span className="card-title details-card-title"><i className="material-icons">person</i> Customer details</span>
      <Field name="name" type="text" component={renderField} label="Name"/>
        <Field name="phone" type="text" component={renderField} label="Mobile"/>
        <Field name="email" type="email" component={renderField} label="Email"/>
        <Field name="delivery_address" type="text" component={renderField} label="Delivery Address"/>
        <Field name="delivery_type" component={renderSelect}  className="browser-default" label="Delivery Type" />
        <Field name="coupon" component={renderField} label="Coupon(optional)" type="text"/>
      

    </div>
    <div className="card-action proceed-margin">
          <Link to="/confirm" className="btn-large waves-effect btn-flat waves-light teal lighten-1" disabled={ submitting || (dirty && invalid)} onClick={handleSubmit(submit.bind(this))}>Proceed</Link>
      </div>
    </form>
  )
  }
  componentDidMount(){
    
  }
}

//const formValues={};

function ore(val){
  return reduxForm({
  form: 'syncValidation',  // a unique identifier for this form
  validate,                // <--- validation function given to redux-form
initialValues :val
})(SyncValidationForm);
}


export default ore;
/*export default  reduxForm({
  form: 'syncValidation',  // a unique identifier for this form
  validate,                // <--- validation function given to redux-form
initialValues :formValues
})(SyncValidationForm)*/