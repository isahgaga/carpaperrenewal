import React from 'react'
import {getBookingDetails,gateway} from '../../services'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import {Link} from 'react-router'
import {connect} from 'react-redux'
import ConfirmStyle from './Confirm.css'
import {BASE_URI_WEB} from '../../services/routes.js'

 class Confirm extends React.Component{
 	constructor(props){
      super(props);
      this.getBookingPapers=this.getBookingPapers.bind(this);
      this.goToGateway=this.goToGateway.bind(this);
      this.state={
      	details:{papers:[]},
                  };
  }
	componentDidMount(){

			this.props.dispatch(showLoading());
			getBookingDetails(this.props.params.tId).then(
				data=>{
					this.props.dispatch(hideLoading());
					this.setState({
				details:data.data
			})

		}
		);
}
goToGateway(){
	this.props.dispatch(showLoading());
	gateway(this.props.params.tId).then(
		data=>{
			this.props.dispatch(hideLoading());
			location.assign(data.data);
		}
		).catch(e=>console.log(e))

}
getBookingPapers(){
	return this.state.details.papers.map(paper=>(
		<li key={paper.id}>
			<span className="name">{paper.title}  x {paper.quantity}</span>
			<span className="price">
			<ccc className="conf-price">&#8358;{paper.price}</ccc>
			</span>
		</li>
		))
}
	render(){
		return (
			<main className="confirm-section">
				<section  >
				{this.state.details.name &&
					<div className="container">
						<div className="row no-margin-bottom">
							<h4 className="section-h">confirm your order</h4>
							<div className="col-md-12">
								<div className="row">
									<div className="col-md-6">
										<div className="card  confirm-card">
								            <div className="card-content">
									              <span className="card-title confirm-title" >Customer Details</span>
									              <span className="my-items">{this.state.details.name}</span>
									              <span className="my-items">{this.state.details.phone_number}</span>
									              <span className="my-items">{this.state.details.email}</span>
									              <span className="my-items">{this.state.details.delivery_address}</span>
								            </div>
							          	</div>
									</div>
									<div className="col-md-6">
										<div className="card confirm-card ">
								            <div className="card-content">
								            	<span className="card-title confirm-title" >Vehicle Details</span>
								              	<ul>
													<li>
														<span className="name">Vehicle Owner</span>
														<span className="price">
														<ccc className="cus-details">{this.state.details.owner_name}</ccc>
													</span>
													</li>
													<li><span className="name">Plate Number</span><span className="price"><ccc  className="conf-price">{this.state.details.plate_number}</ccc></span></li>
													<li><span className="name">Vehicle Make</span><span className="price"><ccc  className="conf-price">{this.state.details.vehicle_make}</ccc></span></li>
													{this.state.details.engine_number && <li><span className="name">Engine Number</span><span className="price"><ccc  className="conf-price">{this.state.details.engine_number}</ccc></span></li>}
													{this.state.details.chasis_number && <li><span className="name">Chasis Number</span><span className="price"><ccc  className="conf-price">{this.state.details.chasis_number}</ccc></span></li>}
													<li><span className="name">Expiry Date</span><span className="price"><ccc  className="conf-price">{this.state.details.expiry_date}</ccc></span></li>
												</ul>
								            </div>
						            
						          		</div>
									</div>
								</div>
								
							</div>
						</div>
						<div className="row no-margin-bottom">
							<div className="col-md-12">
								<div className="card confirm-card order-card">
						            <div className="card-content ">
						              	<span className="card-title confirm-title">Your Order</span>
						              	<ul>
						              		{this.getBookingPapers()}
											<li><span className="name">Cart Subtotal</span><span className="price"><ccc  className="conf-price">&#8358;{this.state.details.original_amount}</ccc></span></li>
											<li><span className="name">Delivery</span><span className="price"><ccc  className="conf-price">&#8358;{this.state.details.delivery_amount}</ccc></span></li>
											<li><span className="name">Order Total</span><span className="price"><ccc  className="conf-price">&#8358;{this.state.details.final_amount}</ccc></span></li>
										</ul>
						            </div>
						            <div className="card-action">
						              <a href={`${BASE_URI_WEB}details`} className="btn-large waves-effect btn-flat waves-light confirm-btn">Back</a>
						              <a className="btn-large waves-effect btn-flat waves-light teal lighten-1 confirm-btn" onClick={this.goToGateway.bind(null)}>Confirm</a>
						            </div>
					          </div>
							</div>
						</div>
					</div>
		}
					
				</section>
			</main>
			);
	}
}
const ShellComponent = connect()(Confirm);
export default ShellComponent;