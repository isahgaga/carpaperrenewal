 import React from 'react'
import styles from './Footer.css'
import {SOCIALS} from '../../services/routes'

export default class Footer extends React.Component{

  render(){
    return (
      <footer className="page-footer teal lighten-1" id="contact">
          <div className="container">
            <div className="row">
              <div className="col l6 s12">
                <h5 className="white-text">Contact us</h5>
                <p className="grey-text text-lighten-4">Suite J2/J3 Ikota Shopping Complex, Ajah, Lagos.</p>
                <p className="grey-text text-lighten-4">+234 903 289 7715</p>
                <p className="grey-text text-lighten-4">info@ogapaper.com</p>
                
              </div>
              
            </div>
          </div>
          <div className="footer-copyright teal ">
            <div className="container">
            © 2017 Copyright
            <div className="right">
              <a className="grey-text text-lighten-4 social-margin" href={SOCIALS.facebook} target="_blank"><i className="fa fa-facebook"></i></a>
              <a className="grey-text text-lighten-4 social-margin" href={SOCIALS.twitter} target="_blank"><i className="fa fa-twitter"></i></a>
              <a className="grey-text text-lighten-4 " href={SOCIALS.instagram} target="_blank"><i className="fa fa-instagram"></i></a>
            </div>
            
            </div>

          </div>
        </footer>
      );
  }
 }
