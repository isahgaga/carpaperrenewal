import React from 'react';
import HeaderStyle from './Header.css';
import AnimateCss from 'Animate.css';
import $ from 'jquery'


export default class Header extends React.Component{
	render(){
		return (<section className="header-section teal lighten-1">
				
				<div className="white-text header-center-box" >
            <h1 className="no-margin-top center bold-text">Renew your car papers</h1>
            <div className="flipping-msg">
	            <h2 className="center no-margin-top">We help you acquire or renew your car papers anywhere in Nigeria.</h2>
	            <h2 className="center no-margin-top animated flipInX">We help you acquire or renew your car papers anywhere in Lagos.</h2>
	            <h2 className="center no-margin-top">We help you acquire or renew your car papers anywhere in Abuja.</h2>
            </div>
            <div className="center get-started-margin-top">
                <a href="#services"  className="btn-large waves-effect waves-light orange">Get Started</a>
            </div>
        </div>
		</section>);
	}

	componentDidMount(){
		
		
		setInterval(()=>{let e=$('.animated.flipInX').removeClass('animated flipInX'); e.next() &&e.next().length?e.next().addClass("animated flipInX"):e.siblings("h2:first").addClass("animated flipInX")


	},4e3);
	}
}
