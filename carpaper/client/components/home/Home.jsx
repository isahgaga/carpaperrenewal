import React from 'react'
import Head from '../header/Header.jsx'
import About from '../about/About.jsx'
import Benefit from '../Benefit/Benefit.jsx'
import Services from '../Services/Services.jsx'
import Testimonial from '../Testimonial/Testimonial.jsx'


export default class Home extends React.Component{
	render(){
		return ( 
			<main>
			<Head/>
			<About/>
			<Benefit/>
			<Services/>
			<Testimonial/>
			</main>
			);
	}

	componentDidMount(){
		const {hash}=location;
		if (hash !=='') {
			document.getElementById(hash.replace('#','')).scrollIntoView();
		}
	}

	
}