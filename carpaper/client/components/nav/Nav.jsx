import React from 'react'
import styles from './Nav.css'
import {Link} from 'react-router'
import 'materialize-css/bin/materialize.css'
import LoadingBar from 'react-redux-loading-bar'
import {BASE_URI_WEB} from '../../services/routes.js'
import Logo from '../../../media/og.png'
import LogoSm from '../../../media/white.png'

export default class Nav extends React.Component{
  constructor(props){
      super(props);
      this.getNumberOfCartItems=this.getNumberOfCartItems.bind(this);
  }
  getNumberOfCartItems(){
   return Object.values(this.props.cart.quantityById).reduce((total, id) =>
      total + id,
      0
    );
    
 

  }
	render(){
		return (
      <div className="navbar-fixed">
              <nav className="teal lighten-1 nav" role="navigation">
    <div className="nav-wrapper container">
    <Link id="logo-container"to="/" className="brand-logo navbar-brand">
    <img src={`${BASE_URI_WEB}dist/${LogoSm}`} height="30px" className="bg-logo"/><span className="st-title"><span>Oga</span>Paper</span>
    <div>
      <img src={`${BASE_URI_WEB}dist/${LogoSm}`} height="30px" className="sm-logo"/>
    </div>
    </Link>
      <ul className="right hide-on-med-and-down">
        <li><a href={`${BASE_URI_WEB}#about`}>About us</a></li>
        <li><a href={`${BASE_URI_WEB}#benefits`}>Benefits</a></li>
        <li><a href={`${BASE_URI_WEB}#services`}>Services</a></li>
        <li><a href={`${BASE_URI_WEB}#testimonial`}>Testimonials</a></li>
        <li><a href={`${BASE_URI_WEB}#contact`}>Contact us</a></li>
        <li><Link to="/cart" className="cart-link"><i className="material-icons cart-icon">shopping_cart</i>
        <span className="new badge white" data-badge-caption="">{this.getNumberOfCartItems()}</span></Link></li>
      </ul>

      <ul id="nav-mobile" className="side-nav">
         <li><a href={`${BASE_URI_WEB}#about`}>About us</a></li>
        <li><a href={`${BASE_URI_WEB}#benefits`}>Benefits</a></li>
        <li><a href={`${BASE_URI_WEB}#services`}>Services</a></li>
        <li><a href={`${BASE_URI_WEB}#testimonial`}>Testimonials</a></li>
        <li><a href={`${BASE_URI_WEB}#contact`}>Contact us</a></li>
        
      </ul>
      <a href="#" data-activates="nav-mobile" className="button-collapse"><i className="material-icons">menu</i></a>
      
      <Link to="/cart" className="cart-btn cart-link"><i className="material-icons cart-icon">shopping_cart</i>
          <span className="new badge white" data-badge-caption="">{this.getNumberOfCartItems()}</span>
      </Link>
    </div>
    <LoadingBar />
  </nav>
      </div>
      );
	}
}