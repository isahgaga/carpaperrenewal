import React from 'react'
import ReceiptStyle from './Receipt.css'
import {connect} from 'react-redux'
import localForage from 'localForage'
import {SOCIALS} from '../../services/routes'

class Receipt extends React.Component{

	componentDidMount(){
		if (document.getElementById('varx').value==='1'){
			localForage.removeItem('Cart').then(location.reload(true))
		}
	}


	render(){
		return (

			<main className="receipt-section">
				<section>
					<div className="container">
						<div className="row no-margin-bottom">
							<div className="col s12 m12">
								<div className="receipt-box-wrapper">
									<div className="receipt-box">
										<h1>Thank you!</h1>
										<div className="center mb-60-receipt">
											<span>Your order was successful. We have sent your receipt to your email. <span className="bold-txt">Transaction #: {this.props.params.tId}</span></span>
											<div className="social-box-receipt center">
											<p>kindly follow us on social media for news on promos and new services</p>
											<a href={SOCIALS.facebook} target="_blank" className="waves-effect waves-circle waves-light btn-floating btn-large">
												<i className="fa fa-facebook"></i>
											</a>
											<a href={SOCIALS.twitter} target="_blank" className="waves-effect waves-circle waves-light btn-floating btn-large">
												<i className="fa fa-twitter"></i>
											</a>
											<a href={SOCIALS.instagram} target="_blank" className="waves-effect waves-circle waves-light btn-floating btn-large">
												<i className="fa fa-instagram"></i>
											</a>
										</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>	
				</section>		
			</main>
			);
	}

}
const ShellComponent = connect()(Receipt);
export default ShellComponent;