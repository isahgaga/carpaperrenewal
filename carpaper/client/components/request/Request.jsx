import React,{Component} from 'react'
import {connect} from 'react-redux';
import localForage from 'localForage';
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import {BASE_URI_WEB} from '../../services/routes.js'


class Request extends Component{
	render(){
		return(
			<main className="checkout-section">
			
				<section>
					<div className="container ">
						<div className="row no-margin-bottom">
						</div>
					</div>	
				</section>		
			</main>			
			)
	}
	componentDidMount(){
		this.props.dispatch(showLoading());
		localForage.getItem('details').then(data=>{
			this.props.dispatch(hideLoading());
			this.props.router.push('/details');
		})
	}
}
const ShellComponent = connect()(Request);
export default ShellComponent;