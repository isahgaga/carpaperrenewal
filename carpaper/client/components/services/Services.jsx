import React from 'react'
import styles from './Services.css'
import {Link} from 'react-router'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addToCart} from '../../actions'

 class Services extends React.Component {
 	constructor(props){
 		super(props);
 		this.getP=this.getP.bind(this);
 		this.addCart=this.addCart.bind(this);
 	}

 	addCart(i,e){
 		e.preventDefault();
 		this.props.addToCart(i);
 	}
 	getP(test){
 		const PAPERS=Object.values(this.props.papers.byId);
 		const ALLPAPERS={suvs:[],cars:[],buses:[]};
 		if (PAPERS.length >0) {
 			PAPERS.reduce((total,current)=> {
 			current.category.toLowerCase() ==='motorcycle'?total.suvs.push(current):current.category.toLowerCase() ==='car'?total.cars.push(current):total.buses.push(current);
 			
 			return total;
 		},ALLPAPERS);
 		}
 		
 	return	ALLPAPERS[test].map(pap=>(

 				 <li className="collection-item" key={pap.id}><span className="services-title">{pap.title}<br/><span className="services-price">&#8358;{pap.price}</span></span><a className="btn btn-flat waves-effect waves-light orange" onClick={this.addCart.bind(null,pap.id)}><i className="material-icons">add_shopping_cart</i></a></li>
 				

 			));
 	}

 
	
	render(){
		return (
			<section className="blue-grey lighten-5 section-padding services" id="services">
				<div className="container ">
					
      				<div className="row">
                              <h4 className="section-h">Our services</h4>
      				<div className="col-lg-4 col-xs-12 col-md-4">
      					<div className="card ns-card">
      					<div className="card-content teal lighten-1 center white">
      						<i className="material-icons">directions_car</i>
      						<h4>Cars</h4>
      					</div>
      					
      					<ul className="collection services-list">
					      
					      {this.getP('cars')}
					    </ul>
					    
      				</div>
      				</div>

      				<div className="col-lg-4 col-xs-12 col-md-4">
      					<div className="card ns-card">
      					<div className="card-content teal lighten-1 center white">
      						<i className="material-icons">directions_car</i>
      						<h4>SUVs</h4>
      					</div>
      					
      					<ul className="collection services-list">
					      {this.getP('suvs')}
					    </ul>
					    
      				</div>
      				</div>
      					
      					<div className="col-lg-4 col-xs-12 col-md-4">
      					<div className="card ns-card">
      					<div className="card-content teal lighten-1 center white">
      						<i className="material-icons">directions_bus</i>
      						<h4>Buses</h4>
      					</div>
      					
      					<ul className="collection services-list">
					    {this.getP('buses')}
					    </ul>
					    
	      				</div>
	      				</div>


      				
      				</div>
      				
				</div>
			</section>
			);
	}
}

function mapStateToProps(state){
	return {
		papers:state.Papers 
	}
}
	
function mapDispatchToProps(dispatch){
	return bindActionCreators({addToCart},dispatch);
}

const ShellComponent = connect(mapStateToProps,mapDispatchToProps)(Services);
export default ShellComponent;