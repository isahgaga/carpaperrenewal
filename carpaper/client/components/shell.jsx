import React from 'react'
import Nav from './nav/Nav.jsx'
import Footer from './footer/Footer.jsx'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addPapers,setCartInitialState} from '../actions'
import {getPapers,getInitialCartState} from '../services'




 class Shell extends React.Component{
	render(){
		return (
			<div className="shell"> 
			<Nav {...this.props}/> 
			{this.props.children}
			<Footer/>
			</div>
			)
	}
	componentDidMount(){
		
		var result;
		getPapers().then(data=>this.props.addPapers(data.data)).catch(error=>console.log(error));
		getInitialCartState.then(val=>this.props.setCartInitialState(val))
	}
}

function mapStateToProps(state){
	return {
		papers:state.Papers,
		cart:state.Cart 
	}
}
	
function mapDispatchToProps(dispatch){
	return bindActionCreators({addPapers,setCartInitialState},dispatch);
}

const ShellComponent = connect(mapStateToProps,mapDispatchToProps)(Shell);
export default ShellComponent;