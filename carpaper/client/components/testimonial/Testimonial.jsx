 import React from 'react'
import styles from './Testimonial.css'
import $ from 'jquery'
import {getTestimonials} from '../../services'

export default class Testimonial extends React.Component{
	
	constructor(props){
      super(props);
      this.testy=this.testy.bind(this);
		this.state={
			testimonials:[]
		};
  }

	testy(){
		return this.state.testimonials.map((item,index)=>{
				return (
					<div className={index===0?"animated fadeInUp":''} key={index}>
					<h2 className="center no-margin-top">{`"${item.testimony}"`}</h2>
					<span>- {item.author}</span>
					</div>
					)
			})
			
	}

  render(){
		
    return (
    	<section className="blue-grey lighten-5  no-margin-top" id="testimonial">
				<div className="container center testimonial-msg">
				{this.testy()}
				</div>
		</section>

    	);

}

componentDidMount(){
		getTestimonials().then(data=>{
				this.setState({testimonials:data.data})
		}).catch(e=>console.log(e));
		setInterval(()=>{let e=$('.animated.fadeInUp').removeClass('animated fadeInUp'); e.next() &&e.next().length?e.next().addClass("animated fadeInUp"):e.siblings("div:first").addClass("animated fadeInUp")},4e3);

	}
}