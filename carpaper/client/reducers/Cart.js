import * as Types from '../constants/ActionTypes'



var initialState = {
  addedIds: [],
  quantityById: {}
}





const addedIds = (state = initialState.addedIds, action) => {
  switch (action.type) {
    case Types.ADD_TO_CART:
      if (state.indexOf(action.paperId) !== -1) {
        return state
      }
      return [ ...state, action.paperId ];
      case Types.REMOVE_FROM_CART:
      const i=state.indexOf(action.id);
      if(i === -1){
        return state;
      }
      
      return [...state.slice(0,i),...state.slice(i+1)]
    default:
      return state
  }
}

const quantityById = (state = initialState.quantityById, action) => {
  switch (action.type) {
    case Types.ADD_TO_CART:
      const { paperId } = action;
      return { 
      	...state,
        [paperId]: (state[paperId] || 0) + 1
      };

      case Types.REMOVE_FROM_CART:
        const I=action.id;
      if(!I){
          return state;
      }
      let result= {...state};
      delete result[I];
      return result;
      
    default:
      return state
  }
}

 const Cart =(state=initialState, action)=>{
  
	switch(action.type){
		    case Types.SETCARTINITIALSTATE:

        return action.initialState?action.initialState:initialState;
    default:
      return {
        addedIds: addedIds(state.addedIds, action),
        quantityById: quantityById(state.quantityById, action)
      }
	}
} 

export default Cart;