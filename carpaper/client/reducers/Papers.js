import * as Types from '../constants/ActionTypes'

const byId = (state = {}, action) => {
  switch (action.type) {
    case Types.ADD_PAPERS:
      return {
        ...state,
        ...action.papers.reduce((obj, paper) => {
          obj[paper.id] = paper
          return obj
        }, {})
      }
    default:
      
      return state
  }
}

 const Papers =(state={byId:{}}, action)=>{
	switch(action.type){
		case  Types.ADD_PAPERS :
		return{
			byId:byId(state.byId,action)	
		} 
		default:
		return state;
	}
} 


export default Papers;

