import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import Cart from './Cart';
import Papers from './Papers';
import { reducer as formReducer } from 'redux-form'
import { loadingBarReducer } from 'react-redux-loading-bar'

const rootReducer = combineReducers({Cart,
									Papers, 
									routing:routerReducer,
									form:formReducer,
									loadingBar: loadingBarReducer,
								});

export default rootReducer;