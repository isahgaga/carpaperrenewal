import * as Routes from './routes.js'
import axios from 'axios'
import localForage from 'localForage'

export const getPapers=()=>axios.get(`${Routes.BASE_URI}${Routes.PAPERS_URL}`);
export const makeBooking=data=>axios.post(`${Routes.BASE_URI}${Routes.BOOKING_URL}`,data);
export const getTestimonials=()=>axios.get(`${Routes.BASE_URI}${Routes.TESTIMONIALS}`);
export const getBenefits=()=>axios.get(`${Routes.BASE_URI}${Routes.BENEFITS}`);
export const getInitialCartState=localForage.getItem('Cart');
export const getDeliveryPrices=()=>axios.get(`${Routes.BASE_URI}${Routes.DELIVERY}`);
export const getBookingDetails=id=>axios.get(`${Routes.BASE_URI+Routes.BOOKING_DETAILS+id}`);
export const gateway=id=>axios.get(`${Routes.BASE_URI+Routes.GATEWAY+id}`);