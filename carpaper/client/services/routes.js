export const BASE_URI="http://ogapaper.com/api/";
export const BASE_URI_WEB="http://ogapaper.com/";
export const PAPERS_URL="index";
export const BOOKING_URL="booking";
export const TESTIMONIALS="customPosts/testimonials";
export const BENEFITS="customPosts/benefits";
export const DELIVERY="delivery";
export const BOOKING_DETAILS="confirmBooking/";
export const GATEWAY='goToGateway/';
export const BASE_NAME='/';
export const SOCIALS={facebook:'https://www.facebook.com/Ogapaper_ng-300549090385442',
					twitter:'https://twitter.com/OgapaperNG',
					instagram:'https://www.instagram.com/ogapaperng/'
				};