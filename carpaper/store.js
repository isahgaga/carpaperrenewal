import {applyMiddleware,createStore,compse} from 'redux'
import {syncHistoryWithStore} from 'react-router-redux'
//import {browserHistory} from 'react-router'
import rootReducer from './client/reducers/index'
import {setInitialState} from './client/middleware'
import { useRouterHistory } from 'react-router'
import { createHistory } from 'history'
import {BASE_NAME} from './client/services/routes.js'


const browserHistory = useRouterHistory(createHistory)({
  basename: BASE_NAME
});
const store = createStore(rootReducer,

	applyMiddleware(setInitialState)
	);
export const history= syncHistoryWithStore(browserHistory,store);

export default store;