/*
    ./webpack.config.js
*/
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './index.html',
  filename: 'index.html',
  inject: 'body'
});
const ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {
  entry: './app.jsx',
  output: {
    path: path.resolve('dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: {loader:"css-loader",
          options:{
            modules:false,
            localIdentName:'[path][name]__[local]--[hash:base64:5]'
          }
        }
        })
      },



      { test: /\.js$/, 
        use: {loader:'babel-loader'},
       exclude: /node_modules/ 
     },
      { test: /\.jsx$/, 
        use: {loader:'babel-loader'},
         exclude: /node_modules/ 
       },
       {
        test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: {loader:'file-loader'},
        include: /node_modules/ 
       },
       
      {
  test: /\.(gif|jpe?g|png|svg)(\?.*)?$/,
  use: [
    'file-loader',
    {
      loader: 'image-webpack-loader',
      options: {}
    }
  ]
}
    ]
    
  },
  plugins: [HtmlWebpackPluginConfig,new ExtractTextPlugin("styles.css")]
}